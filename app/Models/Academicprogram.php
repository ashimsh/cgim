<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Academicprogram extends Model
{
    protected $fillable = ['title','abbr','pic_name', 'semesters', 'description','slug','level_id'];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    public function level()
    {
        return $this->belongsTo(Level::class);
    }

    public function enquiry()
    {
        return $this->hasMany(Enquiry::class);
    }
    
    public function courses()
    {
        return $this->hasMany(Course::class);
    }
}
