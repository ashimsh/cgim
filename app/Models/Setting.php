<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['address', 'email', 'number1', 'number2', 'links'];
}
