<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $fillable = ['title','description','cover-image'];

    public function pictures()
    {
        return $this->hasMany(Picture::class);
    }
}
