<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [ 'subject', 'slug', 'academicprogram_id', 'code', 'classification', 'credit_hour' ];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    public function programs()
    {
        return $this->belongsTo(Academicprogram::class);
    }
}
