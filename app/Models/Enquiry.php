<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $fillable = ['name', 'qualification', 'board', 'marks', 'college', 'phone', 'email', 'address', 'program_id', 'hear', 'referred'];

    public function programs()
    {
        $this->belongsTo(Academicprogram::class);
    }
}
