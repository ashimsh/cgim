<?php

namespace App\Http\Requests\Enquiry;

use Illuminate\Foundation\Http\FormRequest;

class AddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'qualification' => 'required',
            'board' => 'required',
            'marks' => 'required',
            'college' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'applying_for' => 'required',
            'hear' => 'required'
        ];
    }
}
