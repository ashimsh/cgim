<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AcademicProgram;
use App\Models\Level;
use App\Http\Requests\Programs\AddValidation;
use App\Http\Requests\Programs\EditValidation; 

class ProgramController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programs = AcademicProgram::all();
        $levels = Level::all();
        return view('dashboard.programs.index',compact('programs','levels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $levels = Level::all();
        return view('dashboard.programs.create',compact('levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddValidation $request)
    {
        $levels = Level::all();
        $data = $request->all();
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'programs', $imageName);
        $data['pic_name'] = $imageName;
        AcademicProgram::create($data);
        return redirect()->route('dashboard.programs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $programs = AcademicProgram::findorFail($id);
        return view('dashboard.programs.show',compact('programs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $levels = Level::all();
        $programs = AcademicProgram::findorFail($id);
        return view('dashboard.programs.edit',compact('programs','levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditValidation $request, $id)
    {
        $program = AcademicProgram::findorFail($id);
        $program->title = $request->get('title');
        $program->abbr = $request->get('abbr');
        $data = $request->all();
        if($request->hasfile('image'))
        {
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'programs', $imageName);
            if(file_exists(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'programs'.DIRECTORY_SEPARATOR.$program->pic_name))
            {
                unlink(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'programs'.DIRECTORY_SEPARATOR.$program->pic_name);
            }
            $data['pic_name'] = $imageName;
        }
        $program->description = $request->get('description');
        $program->update();

        return redirect()->route('dashboard.programs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program = AcademicProgram::findorFail($id);
        if(file_exists(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'programs'.DIRECTORY_SEPARATOR.$program->pic_name))
        {
            unlink(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'programs'.DIRECTORY_SEPARATOR.$program->pic_name);
        }
        $program->delete();

        return redirect()->route('dashboard.programs');
    }
}
