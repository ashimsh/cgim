<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\ContactMail;
use App\Mail\ReplyMail;
use App\Models\Contact;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\Contact\AddValidation;
use App\Http\Requests\Contact\EditValidation;

class ContactController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    // public function messages()
    // {
    //     return view('dashboard.includes.message',compact('contacts'));
    // }
    public function index()
    {
        $messages = Contact::all();
        return view('dashboard.contact.index',compact('messages'));
    }
    public function create()
    {
        return view('check');
    }

    public function store(AddValidation $request)
    {
       $contact = Contact::create($request->all());
       
       Mail::to(ENV('MAIL_USERNAME'))->send(new ContactMail($contact));
       
       return redirect()->back();
       
    } 

    public function show($id)
    {
        $contact = Contact::findorFail($id);
        $contact->status = 1;
        $contact->update();
        return view('dashboard.contact.show',compact('contact'));
    }
    
    public function destroy($id)
    {
        $contact = Contact::findorFail($id);
        $contact->delete();
        
        return redirect()->route('dashboard.home');
    }
    
    public function reply(EditValidation $request, $id)
    {
        $contact = Contact::findorFail($id);
        $reply = $request->get('reply');

        Mail::to($contact->email)->send(new ReplyMail($reply));
        return redirect()->back()->with('success','Sent!');
    } 
}
