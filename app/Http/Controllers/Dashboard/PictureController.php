<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Album;
use App\Models\Picture;


class PictureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pictures = Picture::all();
        return view('dashboard.pictures.index',compact('pictures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $album = Album::findorFail($id);
        return view('dashboard.pictures.create',compact('album'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $imageName = time().'.'.$data['file']->getClientOriginalExtension();
        $data['file']->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'photo-gallery', $imageName);     
        $data['name'] = $imageName;
        Picture::create($data);
        return redirect()->route('dashboard.album')
        ->with('success','Images Successfully Uploaded!')
        ->with('image',$imageName);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $picture = Picture::findorFail($id);
        $album = Album::find($picture->album->id);

        if ($picture->cover == 0 )
        {
            $picture->cover = 1;
            $picture->update();

            foreach($album->pictures as $pic){
                if($pic->cover = 1 and $pic->id != $id){
                    $pic->cover = 0;
                    $pic->update();
                }
            }
        }
        return redirect()->route('dashboard.album');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $picture = Picture::findorFail($id);
        if(file_exists(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'photo-gallery'.DIRECTORY_SEPARATOR.$picture->name))
        {
            unlink(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'photo-gallery'.DIRECTORY_SEPARATOR.$picture->name);
        }
        $picture->delete();

        return redirect()->back();
    }
}
