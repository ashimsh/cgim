<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Slider\AddValidation;
use App\Http\Requests\Slider\EditValidation;
use App\Models\Slider;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $img = Slider::all();
         return view('dashboard.slider.index',compact('img'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddValidation $request)
    { 
        $data = $request->all();
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'slider', $imageName);
        $data['name'] = $imageName;
        Slider::create($data);
        return redirect()->route('dashboard.slider')
        ->with('success','Image Successfully Uploaded!')
        ->with('image',$imageName);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pic = Slider::findorFail($id);
        return view('dashboard.slider.show',compact('pic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pic = Slider::findorFail($id);
        return view('dashboard.slider.edit',compact('pic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditValidaiton $request, $id)
    {
        $pic = Slider::findorFail($id);
        $data = $request->all();
        if($request->hasfile('image'))
        {
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'slider', $imageName);
            if(file_exists(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'slider'.DIRECTORY_SEPARATOR.$pic->name))
            {
                unlink(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'slider'.DIRECTORY_SEPARATOR.$pic->name);
            }
            $data['name'] = $imageName;
        }
        
        $pic-> update($data);

        return redirect()->route('dashboard.slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $img = Slider::findorFail($id);
        if(file_exists(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'slider'.DIRECTORY_SEPARATOR.$img->name))
        {
            unlink(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'slider'.DIRECTORY_SEPARATOR.$img->name);
        }
        $img->delete();

        return redirect()->back();
    }
}