<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'dashboard/', 'as' => 'dashboard.', 'namespace' => 'Dashboard\\'], function()
{
    Route::get('home',                  ['as' => 'home',          'uses' => 'DashboardController@index']);

    Route::get('slider',                ['as' => 'slider',        'uses' => 'SliderController@index']);
    Route::get('slider/create',         ['as' => 'slider.create', 'uses' => 'SliderController@create']);
    Route::post('slider/store',         ['as' => 'slider.store',  'uses' => 'SliderController@store']);
    Route::get('slider/show/{id}',      ['as' => 'slider.show',   'uses' => 'SliderController@show']);
    Route::get('slider/edit/{id}',      ['as' => 'slider.edit',   'uses' => 'SliderController@edit']);
    Route::put('slider/update/{id}',    ['as' => 'slider.update', 'uses' => 'SliderController@update']);
    Route::delete('slider/delete/{id}', ['as' => 'slider.delete', 'uses' => 'SliderController@destroy']);

    Route::get('programs',                ['as' => 'programs',        'uses' => 'ProgramController@index']);
    Route::get('programs/create',         ['as' => 'programs.create', 'uses' => 'ProgramController@create']);
    Route::post('programs/store',         ['as' => 'programs.store',  'uses' => 'ProgramController@store']);
    Route::get('programs/show/{id}',      ['as' => 'programs.show',   'uses' => 'ProgramController@show']);
    Route::get('programs/edit/{id}',      ['as' => 'programs.edit',   'uses' => 'ProgramController@edit']);
    Route::put('programs/update/{id}',    ['as' => 'programs.update', 'uses' => 'ProgramController@update']);
    Route::delete('programs/delete/{id}', ['as' => 'programs.delete', 'uses' => 'ProgramController@destroy']);

    Route::get('courses',                ['as' => 'courses',        'uses' => 'CourseController@index']);
    Route::get('courses/create/{id}',         ['as' => 'courses.create', 'uses' => 'CourseController@create']);
    Route::post('courses/store',         ['as' => 'courses.store',  'uses' => 'CourseController@store']);
    Route::get('courses/show/{id}',      ['as' => 'courses.show',   'uses' => 'CourseController@show']);
    Route::get('courses/edit/{id}',      ['as' => 'courses.edit',   'uses' => 'CourseController@edit']);
    Route::put('courses/update/{id}',    ['as' => 'courses.update', 'uses' => 'CourseController@update']);
    Route::delete('courses/delete/{id}', ['as' => 'courses.delete', 'uses' => 'CourseController@destroy']);

    Route::get('level',                ['as' => 'level',        'uses' => 'LevelController@index']);
    Route::get('level/create',         ['as' => 'level.create', 'uses' => 'LevelController@create']);
    Route::post('level/store',         ['as' => 'level.store',  'uses' => 'LevelController@store']);
    Route::get('level/show/{id}',      ['as' => 'level.show',   'uses' => 'LevelController@show']);
    Route::get('level/edit/{id}',      ['as' => 'level.edit',   'uses' => 'LevelController@edit']);
    Route::put('level/update/{id}',    ['as' => 'level.update', 'uses' => 'LevelController@update']);
    Route::delete('level/delete/{id}', ['as' => 'level.delete', 'uses' => 'LevelController@destroy']);

    Route::get('contact',                ['as' => 'contact',        'uses' => 'ContactController@index']);
    Route::get('contact/create',         ['as' => 'check',          'uses' => 'ContactController@create']);
    Route::post('contact/store',         ['as' => 'contact.store',  'uses' => 'ContactController@store']);
    Route::get('contact/show/{id}',      ['as' => 'contact.show',   'uses' => 'ContactController@show']);
    Route::delete('contact/delete/{id}', ['as' => 'contact.delete', 'uses' => 'ContactController@destroy']);
    Route::post('contact/reply/{id}',     ['as' => 'contact.reply', 'uses' => 'ContactController@reply']);

    Route::get('album',                ['as' => 'album',        'uses' => 'AlbumController@index']);
    Route::get('album/create',         ['as' => 'album.create', 'uses' => 'AlbumController@create']);
    Route::post('album/store',         ['as' => 'album.store',  'uses' => 'AlbumController@store']);
    Route::get('album/show/{id}',      ['as' => 'album.show',   'uses' => 'AlbumController@show']);
    Route::get('album/edit/{id}',      ['as' => 'album.edit',   'uses' => 'AlbumController@edit']);
    Route::put('album/update/{id}',    ['as' => 'album.update', 'uses' => 'AlbumController@update']);
    Route::delete('album/delete/{id}', ['as' => 'album.delete', 'uses' => 'AlbumController@destroy']);

    Route::get('pictures',                ['as' => 'pictures',        'uses' => 'PictureController@index']);
    Route::get('pictures/create/{id}',    ['as' => 'pictures.create', 'uses' => 'PictureController@create']);
    Route::post('pictures/store',         ['as' => 'pictures.store',  'uses' => 'PictureController@store']);
    Route::get('pictures/show/{id}',      ['as' => 'pictures.show',   'uses' => 'PictureController@show']);
    Route::get('pictures/edit/{id}',      ['as' => 'pictures.edit',   'uses' => 'PictureController@edit']);
    Route::put('pictures/update/{id}',    ['as' => 'pictures.update', 'uses' => 'PictureController@update']);
    Route::delete('pictures/delete/{id}', ['as' => 'pictures.delete', 'uses' => 'PictureController@destroy']);

    Route::get('video',                ['as' => 'video',        'uses' => 'VideoController@index']);
    Route::get('video/create',         ['as' => 'video.create', 'uses' => 'VideoController@create']);
    Route::post('video/store',         ['as' => 'video.store',  'uses' => 'VideoController@store']);
    Route::get('video/show/{id}',      ['as' => 'video.show',   'uses' => 'VideoController@show']);
    Route::get('video/edit/{id}',      ['as' => 'video.edit',   'uses' => 'VideoController@edit']);
    Route::put('video/update/{id}',    ['as' => 'video.update', 'uses' => 'VideoController@update']);
    Route::delete('video/delete/{id}', ['as' => 'video.delete', 'uses' => 'VideoController@destroy']);

    Route::get('page',                ['as' => 'page',        'uses' => 'PageController@index']);
    Route::get('page/create',         ['as' => 'page.create', 'uses' => 'PageController@create']);
    Route::post('page/store',         ['as' => 'page.store',  'uses' => 'PageController@store']);
    Route::get('page/show/{id}',      ['as' => 'page.show',   'uses' => 'PageController@show']);
    Route::get('page/edit/{id}',      ['as' => 'page.edit',   'uses' => 'PageController@edit']);
    Route::put('page/update/{id}',    ['as' => 'page.update', 'uses' => 'PageController@update']);
    Route::delete('page/delete/{id}', ['as' => 'page.delete', 'uses' => 'PageController@destroy']);

    Route::get('about',                ['as' => 'about',        'uses' => 'AboutController@index']);
    Route::get('about/create',         ['as' => 'about.create', 'uses' => 'AboutController@create']);
    Route::post('about/store',         ['as' => 'about.store',  'uses' => 'AboutController@store']);
    Route::get('about/show/{id}',      ['as' => 'about.show',   'uses' => 'AboutController@show']);
    Route::get('about/edit/{id}',      ['as' => 'about.edit',   'uses' => 'AboutController@edit']);
    Route::put('about/update/{id}',    ['as' => 'about.update', 'uses' => 'AboutController@update']);
    Route::delete('about/delete/{id}', ['as' => 'about.delete', 'uses' => 'AboutController@destroy']);

    Route::get('settings',                ['as' => 'settings',        'uses' => 'SettingController@index']);
    Route::get('settings/create',         ['as' => 'settings.create', 'uses' => 'SettingController@create']);
    Route::post('settings/store',         ['as' => 'settings.store',  'uses' => 'SettingController@store']);
    Route::get('settings/show/{id}',      ['as' => 'settings.show',   'uses' => 'SettingController@show']);
    Route::get('settings/edit/{id}',      ['as' => 'settings.edit',   'uses' => 'SettingController@edit']);
    Route::put('settings/update/{id}',    ['as' => 'settings.update', 'uses' => 'SettingController@update']);
    Route::delete('settings/delete/{id}', ['as' => 'settings.delete', 'uses' => 'SettingController@destroy']);

    Route::get('enquiry',                ['as' => 'enquiry',        'uses' => 'EnquiryController@index']);
    Route::get('enquiry/create',         ['as' => 'enquiry.create', 'uses' => 'EnquiryController@create']);
    Route::post('enquiry/store',         ['as' => 'enquiry.store',  'uses' => 'EnquiryController@store']);
    Route::get('enquiry/show/{id}',      ['as' => 'enquiry.show',   'uses' => 'EnquiryController@show']);
    Route::get('enquiry/edit/{id}',      ['as' => 'enquiry.edit',   'uses' => 'EnquiryController@edit']);
    Route::put('enquiry/update/{id}',    ['as' => 'enquiry.update', 'uses' => 'EnquiryController@update']);
    Route::delete('enquiry/delete/{id}', ['as' => 'enquiry.delete', 'uses' => 'EnquiryController@destroy']);
});
    
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
