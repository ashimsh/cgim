<?php

use Illuminate\Database\Seeder;

class AlbumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('albums')->insert([

            [
                'title' => 'Farewell 2019',
                'description' => 'This was a farewell program for the batch of 2015'
            ],

            [
                'name' => 'Holi 2019',
                'description' => 'Festival of holi in the year 2019'
            ],

            [
                'name' => 'Outhouse orientation',
                'description' => ''
            ]
        ]);
    }
}
