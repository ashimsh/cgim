<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->insert([

            [
                'name' => 'ashim shrestha',
                'email' => 'asiemspock@gmail.com',
                'message' => 'this is a message'
            ],

            [
                'name' => 'manish basnyat',
                'email' => 'manish182@gmail.com',
                'message' => 'this is another message'
            ],

            [
                'name' => 'manish shrestha',
                'email' => 'manishs@gmail.com',
                'message' => 'this is third message'
            ]
        ]);
    }
}
