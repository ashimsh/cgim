<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquiries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('qualification');
            $table->string('board');
            $table->integer('marks');
            $table->string('college');
            $table->bigInteger('phone');
            $table->string('email');
            $table->string('address');
            $table->unsignedBigInteger('academicprogram_id');
            $table->string('hear');
            $table->string('referred')->nullable();
            $table->timestamps();
            $table->foreign('academicprogram_id')->references('id')->on('academicprograms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enquiries');
    }
}
