<div class="form-group">
    {!! Form::open(['route' => 'dashboard.contact.store']) !!}
        {!! Form::text('name',null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
        {!! Form::text('email',null, ['placeholder' => 'Email', 'class' => 'form-control']) !!}
        {!! Form::textarea('message',null, ['col' => '30', 'row' => '10', 'placeholder' => 'Message', 'class' => 'form-control']) !!}
        {!! Form::submit('Submit',null) !!}
    {!! Form::close() !!}
</div>
