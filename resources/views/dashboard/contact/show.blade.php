@extends('dashboard.master')

@section('title')
    Contact
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h2>Messages</h2>    
            </div>   
            <button class="btn btn-danger" data-toggle = "modal" data-target = "#contact-{{$contact->id}}" title="Delete" style="float: right"><i class="fa fa-trash"></i></button>
            <div class="modal fade" id="contact-{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Are you sure?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            {!! Form::model($contact, ['route' => ['dashboard.contact.delete',$contact->id]]) !!}
                                @method('DELETE')
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger float-right']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::model($contact, ['route' => ['dashboard.contact.reply',$contact->id]]) !!}
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('name', 'Name') !!}
                            {!! Form::text('name', null, ['class' => 'form-control', 'disabled']) !!}
                            {!! Form::label('email', 'Email') !!}
                            {!! Form::text('email', null, ['class' => 'form-control', 'disabled']) !!}
                            {!! Form::label('message', 'Message') !!}
                            {!! Form::textarea('message', null, ['class' => 'form-control', 'disabled']) !!}
                        </div>
                        <div class="col-sm-6">
                            {!! Form::label('reply', 'Reply') !!}
                            {!! Form::textarea('reply', null, ['class' => 'form-control tiny']) !!}
                            {!! Form::button('Send', ['type' => 'submit', 'class' => 'btn btn-primary', 'style' => 'float: right']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection