@extends('dashboard.master')

@section('title')
    Messages
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3>Messages</h3>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Message</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($messages as $message)
                        <tr>
                            <td> <a href="{{route('dashboard.contact.show',$message->id)}}">{{$message->name}}</a> </td>
                            <td> {{$message->email}} </td>
                            <td> {{$message->message}} </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
@endsection