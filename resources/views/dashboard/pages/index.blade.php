@extends('dashboard.master')

@section('title')
    Pages
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"> Pages </h3>
            <h6 class="float-right"><a href="{{route('dashboard.page.create')}}" class="btn btn-info btn-sm">Create a new page </a> </h6>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pages as $page)
                        <tr>
                            <td> {{$page->title}} </td>
                            <td> {{$page->description}} </td>
                            <td>
                                <a href="{{route('dashboard.page.edit',$page->id)}}" class="btn btn-primary" title="Edit"> <i class="fa fa-edit"></i> </a>
                                <button class="btn btn-danger" data-toggle = "modal" data-target = "#page-{{$page->id}}" title="Delete"><i class="fa fa-trash"></i></button>
                                <div class="modal fade" id="page-{{$page->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                {!! Form::model($page, ['route' => ['dashboard.page.delete',$page->id]]) !!}
                                                    @method('DELETE')
                                                     {!! Form::submit('Delete', ['class' => 'btn btn-danger float-right']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection