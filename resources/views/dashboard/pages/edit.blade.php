@extends('dashboard.master')

@section('title')
    Edit Page
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit this page</h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::model($page, ['route' => ['dashboard.page.update',$page->id], 'enctype'=>'multipart/form-data']) !!}
                    @method('PUT')
                    @include('dashboard.pages.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>            
@endsection