@extends('dashboard.master')

@section('title')
    New Page
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Add a new Page</h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::open(['route' => 'dashboard.page.store']) !!}
                    @include('dashboard.pages.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>
@endsection
