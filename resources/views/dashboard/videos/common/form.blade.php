{!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
{!! Form::text('title', null, ['class' => 'form-control', 'required'] ) !!}
<br>
{!! Form::label('link', 'Copy your links here', ['class' => 'control-label']) !!}
{!! Form::text('link', null, ['class' => 'form-control', 'required'] ) !!}
<br> 
{!! Form::submit('Submit', ['class' => 'btn btn-primary row mb-0', 'style' => 'float:right']) !!}