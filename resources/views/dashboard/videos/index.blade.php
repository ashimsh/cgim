@extends('dashboard.master')

@section('title')
    Videos
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="card-title"> Videos</h1>
            <a href="{{route('dashboard.video.create')}}" class="btn btn-primary" style="float: right">Upload video</a>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                    <tr>
                        <th>Event name</th>
                        <th>Video</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($videos as $video)
                        <tr>
                            <td> {{$video->title}} </td>
                            <td> {!! $video->link !!} </td>
                            <td>
                                <a href="{{route('dashboard.video.edit',$video->id)}}" class="btn btn-primary" title="Edit"> <i class="fa fa-edit"></i> </a> <hr>
                                <button class="btn btn-danger" data-toggle = "modal" data-target = "#video-{{$video->id}}" title="Delete"><i class="fa fa-trash"></i></button>
                                <div class="modal fade" id="video-{{$video->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                {!! Form::model($video, ['route' => ['dashboard.video.delete',$video->id]]) !!}
                                                    @method('DELETE')
                                                     {!! Form::submit('Delete', ['class' => 'btn btn-danger float-right']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection