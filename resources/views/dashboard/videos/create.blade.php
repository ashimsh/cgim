@extends('dashboard.master')

@section('title')
    New Video
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                Upload Videos
            </div>
            <div class="card-body">
                {!! Form::open(['route' => 'dashboard.video.store']) !!}
                    @include('dashboard.videos.common.form') 
                {!! Form::close() !!}
            </div>
        </div>
    </div>    
</div>
@endsection