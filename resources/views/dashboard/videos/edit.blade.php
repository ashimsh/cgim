@extends('dashboard.master')

@section('title')
    Edit Video
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                Edit Videos
            </div>
            <div class="card-body">
                {!! Form::model($video,['route' => ['dashboard.video.update',$video->id]]) !!}
                    @method('PUT')
                    @include('dashboard.videos.common.form') 
                {!! Form::close() !!}
            </div>
        </div>
    </div>    
</div>
@endsection