@extends('dashboard.master')

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet">
<meta title="csrf-token" content="{{csrf_token()}}">
@endsection

@section('title')
    Upload pictures
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Upload Pictures in <b> {{$album->title}} </b> </h1>
                {!! Form::open([ 'route' => [ 'dashboard.pictures.store' ], 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'imageUpload' ]) !!}
                    <input type="hidden" name="album_id" value="{{$album->id}}">
                {!! Form::close() !!}
                <button type="submit" class="btn btn-primary"  id = "button"> Upload </button>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

   <script type="text/javascript">
        Dropzone.options.imageUpload = {            
            autoProcessQueue:false,
            required:true,
            acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
            addRemoveLinks: true,
            maxFiles:8,
            parallelUploads : 100,
            maxFilesize:10,
            headers: { 'X-CSRF-TOKEN': $('meta[title="csrf-token"]').attr('content') },
            url:"{{ route('dashboard.pictures.store',$album->id) }}",
            init: function(){
                myDropZone = this;
                $("#button").click(function (e) {
                    e.preventDefault();
                    myDropZone.processQueue();
                    
                });
                $(myDropZone).on('sending', function(file, xhr, formData) {
                    // Append all form inputs to the formData Dropzone will POST
                    var data = $('#frmTarget').serializeArray();
                    $.each(data, function(key, el) {
                        formData.append(el.title, el.value);
                    });
                });
            },
        };
        Dropzone.options.imageUpload.init();
</script>
@endsection