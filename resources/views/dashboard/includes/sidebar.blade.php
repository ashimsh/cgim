<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="#" class="brand-link">
    <img src="{{asset('backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Admin Page</span>
  </a>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" role="menu" data-accordion="false" data-widget="treeview">
      <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="{{route('dashboard.slider')}}" class="nav-link @if(request()->is('dashboard/slider')) active @endif">
            <i class="nav-icon fas fa-th"></i>
            <p>Slider</p> 
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-book-open"></i>
            <p>
              Levels
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          @foreach($levels as $level)
            <ul class="nav nav-treeview">
              <li class="nav-item"> 
                <a href="{{route('dashboard.level.show',$level->id)}}" class="nav-link @if(request()->is('dashboard/level/show/'.$level->id)) active @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p> {{$level->title}} </p>
                </a> 
              </li>
            </ul>
          @endforeach
        </li>
        <!-- <li class="nav-item">
          <a href="{{route('dashboard.programs')}}" class="nav-link @if(request()->is('dashboard/programs')) active @endif">
            <i class="nav-icon fas fa-th"></i>
            <p>Programs</p>
          </a>
        </li> -->
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon far fa-image"></i>
              <p>
                Gallery
                <i class="right fas fa-angle-left"></i>
              </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item"> 
              <a href="{{route('dashboard.album')}}" class="nav-link @if(request()->is('dashboard/album')) active @endif">
                <i class="far fa-circle nav-icon"></i>
                <p>Photo Gallery</p>
              </a> 
            </li>
            <li class="nav-item"> 
              <a href="{{route('dashboard.video')}}" class="nav-link @if(request()->is('dashboard/video')) active @endif">
                <i class="far fa-circle nav-icon"></i>
                <p>Video Gallery</p>
              </a> 
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="{{route('dashboard.settings')}}" class="nav-link @if(request()->is('dashboard/settings')) active @endif">
            <i class="nav-icon fas fa-cogs"></i>
            <p>Settings</p> 
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('dashboard.page')}}" class="nav-link @if(request()->is('dashboard/page')) active @endif">
            <i class="nav-icon fas fa-book"></i>
            <p>Pages</p> 
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('dashboard.contact')}}" class="nav-link @if(request()->is('dashboard/contact')) active @endif">
            <i class="nav-icon fas fa-envelope"></i>
            <p>Messages</p> 
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('dashboard.about')}}" class="nav-link @if(request()->is('dashboard/about')) active @endif">
            <i class="nav-icon fas fa-th"></i>
            <p>About</p> 
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('password.request')}}" class="nav-link @if(request()->is('dashboard/password/request')) active @endif">
            <i class="nav-icon fas fa-key"></i>
            <p>Change Password</p> 
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>

