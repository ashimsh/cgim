@if($contacts->count() != 0)
<a class="nav-link" data-toggle="dropdown" href="#">
  <i class="far fa-comments"></i>
  <span class="badge badge-danger navbar-badge"> {{$contacts->count()}} </span> 
</a>
@endif
<div class="dropdown-menu dropdown-menu-right">
  <div class="vertical-menu">
    @foreach($contacts as $contact)
      <div class="media">
        <a href="{{route('dashboard.contact.show',$contact->id)}}">
          <i class="fas fa-user"></i>
          <div class="media-body">
            <h3 class="dropdown-item-title">
              {{$contact->name}}
            </h3>
            <p class="text-sm"> {{$contact->message}} </p>
            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> {{$contact->created_at}} </p>
          </div>
        </a>
      </div>
    @endforeach
  </div>
</div>