<div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{route('dashboard.home')}}">Dashboard</a></li>
        @foreach(Request::segments() as $segment)
        <li class="breadcrumb-item active"> {{$segment}} </li>
        @endforeach
    </ol>
</div>