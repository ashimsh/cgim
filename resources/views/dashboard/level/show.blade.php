@extends('dashboard.master')

@section('title')
    Level
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"> {{$level->title}} level </h3>
            <a href="{{route('dashboard.programs.create')}}" class="btn btn-primary" style="float: right">Add a new program</a>
        </div>
        <div class="card-body">
            <ul>
                @foreach($level->programs as $program)
                    <li> <a href="{{route('dashboard.programs.show',$program->id)}}"> {{$program->title}} </a> </li>      
                @endforeach
            </ul>
        </div>     
    </div>
@endsection