@extends('dashboard.master')

@section('title')
    Levels
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Levels</h3>
            <h6 class="float-right"><a href="{{route('dashboard.level.create')}}" class="btn btn-info btn-sm">Upload a new level</a> </h6>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                    <tr>
                        <th>Levels</th>
                        <th>Programs</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($levels as $level)
                        <tr>
                            <td> <a href="{{route('dashboard.level.show',$level->id)}}">{{$level->title}}</a> </td>
                            <td> @foreach($level->programs as $program) <a href="{{route('dashboard.programs.show',$program->id)}}"> {{$program->title}} <br> @endforeach </a> </td>
                            <td>
                                <a href="{{route('dashboard.level.edit',$level->id)}}" title="Edit" class="btn btn-primary"> <i class="fa fa-edit"></i> </a>
                                <button class="btn btn-danger" data-toggle = "modal" data-target = "#level-{{$level->id}}" title="Delete"><i class="fa fa-trash"></i></button>
                                <div class="modal fade" id="level-{{$level->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                {!! Form::model($level, ['route' => ['dashboard.level.delete',$level->id]]) !!}
                                                    @method('DELETE')
                                                     {!! Form::submit('Delete', ['class' => 'btn btn-danger float-right']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach 
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> 
                        </div>
                    @endif   
                </tbody>
            </table>
        </div>
    </div>
@endsection