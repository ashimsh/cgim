@extends('dashboard.master')

@section('title')
    Edit Level
@endsection

@section('content')
<div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit this level</h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::model($level, ['route' => ['dashboard.level.update',$level->id], 'role' => 'form']) !!}
                    @method('PUT')
                    @include('dashboard.level.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>            
@endsection