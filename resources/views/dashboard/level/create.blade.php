@extends('dashboard.master')

@section('title')
    Add new level
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Add a new Level</h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::open(['route' => 'dashboard.level.store']) !!}
                    @include('dashboard.level.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>
@endsection
