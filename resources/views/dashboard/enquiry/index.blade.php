@extends('dashboard.master')

@section('title')
    Enquiry
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"> Pages </h3>
            <a href="{{route('dashboard.enquiry.create')}}" class="btn btn-primary" style="float: right"> Create a new enquiry </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Applying for</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($enquiries as $enquiry )
                        <tr>
                            <td> {{$enquiry->name}} </td>
                            <td> {{$enquiry->email}} </td>
                            <td> {{$enquiry->academicprogram_id}} </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection