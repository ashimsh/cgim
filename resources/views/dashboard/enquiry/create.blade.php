@extends('dashboard.master')

@section('title')
    Enquiry model
@endsection

@section('content')
    <button class="btn btn-primary" data-toggle = "modal" data-target = "#enquiry">Enquiry Model</button>
    <div class="modal fade" id="enquiry" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Student Enquiry Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route' => ['dashboard.enquiry.store']]) !!}
                        {!! Form::label('name', 'Full Name') !!}
                        {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
                        {!! Form::label('qualification', 'Qualification') !!}
                        {!! Form::text('qualification', null, ['class' => 'form-control', 'required']) !!}
                        {!! Form::label('board', 'University/Board') !!}
                        {!! Form::text('board', null, ['class' => 'form-control', 'required']) !!}
                        {!! Form::label('marks', 'Percentage/GPA') !!}
                        {!! Form::text('marks', null, ['class' => 'form-control', 'required']) !!}
                        {!! Form::label('college', 'Name of College') !!}
                        {!! Form::text('college', null, ['class' => 'form-control', 'required']) !!}
                        {!! Form::label('phone', 'Mobile/phone') !!}
                        {!! Form::number('phone', null, ['class' => 'form-control', 'required']) !!}
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
                        {!! Form::label('address', 'Permanent Address') !!}
                        {!! Form::text('address', null, ['class' => 'form-control', 'required']) !!}
                        {!! Form::label('applying_for', 'Apply For') !!} <br>
                        @foreach($programs as $program)
                            {!! Form::label('applying_for', $program->abbr) !!} 
                            {!! Form::radio('applying_for', $program->abbr, ['class' => 'form-control']) !!} <br>
                        @endforeach
                        {!! Form::label('hear', 'How did you hear about us?') !!} <br>
                        {!! Form::checkbox('hear', 'Newspaper') !!} 
                        {!! Form::label('hear', 'Newspaper') !!}
                        {!! Form::checkbox('hear', 'Television') !!} 
                        {!! Form::label('hear', 'Television') !!}
                        {!! Form::checkbox('hear', 'Friends') !!} 
                        {!! Form::label('hear', 'Friends') !!}
                        {!! Form::checkbox('hear', 'Social Network') !!} 
                        {!! Form::label('hear', 'Social Network') !!} <br>
                        {!! Form::checkbox('hear', 'Faculty/Teacher') !!} 
                        {!! Form::label('hear', 'Faculty/Teacher') !!} <br>
                        {!! Form::label('hear', 'Other') !!}
                        {!! Form::text('hear', null, ['class' => 'form-control']) !!}
                        {!! Form::label('referred', 'Referred by') !!}
                        {!! Form::text('referred', null, ['class' => 'form-control']) !!}
                </div>
                <div class="modal-footer">
                        {!! Form::submit('Close', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) !!}
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                        {!! Form::submit('Submit', ['class' => 'btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection