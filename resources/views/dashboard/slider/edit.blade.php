@extends('dashboard.master')

@section('title')
    Edit Slider
@endsection

@section('content')
<div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit your Slider</h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::model($pic, ['route' => ['dashboard.slider.update',$pic->id],'enctype'=>'multipart/form-data', 'role' => 'form']) !!}
                    @method('PUT')
                    @include('dashboard.slider.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>            
@endsection