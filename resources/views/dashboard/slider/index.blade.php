@extends('dashboard.master')

@section('title')
 Slider
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Slider Pictures</h3>
            <h6 class="float-right"><a href="{{route('dashboard.slider.create')}}" class="btn btn-info btn-sm">Upload a new image in your Slider</a> </h6>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Caption</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($img as $pic)
                        <tr>
                            <td> <a href="{{route('dashboard.slider.show',$pic->id)}}"><img class="img-fluid img-size" src="{{asset('images/slider/'.$pic->name)}}"></a>  </td>
                            <td> <a href="{{route('dashboard.slider.show',$pic->id)}}">{{$pic->caption}}</a> </td>
                            <td>
                                <a href="{{route('dashboard.slider.edit',$pic->id)}}" title="Edit" class="btn btn-primary"> <i class="fa fa-edit"></i> </a>
                                <button class="btn btn-danger" data-toggle = "modal" data-target = "#slider-{{$pic->id}}" title="Delete"><i class="fa fa-trash"></i></button>
                                <div class="modal fade" id="slider-{{$pic->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                {!! Form::model($pic, ['route' => ['dashboard.slider.delete',$pic->id]]) !!}
                                                    @method('DELETE')
                                                     {!! Form::submit('Delete', ['class' => 'btn btn-danger float-right']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach 
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Please select an image first.  
                        </div>
                    @endif   
                </tbody>
            </table>
        </div>
    </div>
@endsection
