@extends('dashboard.master')

@section('title')
    {{$pic->name}}
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{$pic->name}}</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <img src="{{asset('images/slider/'.$pic->name)}}" class="img-fluid img-size">
                </div>
                <div class="col-sm-6">
                    <label for="caption">Caption</label>
                    <p>{{$pic->caption}}</p>
                </div>
            </div>
        </div>
    </div>
@endsection