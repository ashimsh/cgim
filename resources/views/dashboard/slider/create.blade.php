@extends('dashboard.master')

@section('title')
    Upload a pic in Slider
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Add a new Image in your Slider</h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::open(['route' => 'dashboard.slider.store','enctype'=>'multipart/form-data']) !!}
                    @include('dashboard.slider.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>
@endsection
