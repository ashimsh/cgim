<div class="card-body">
  <div class="form-group">
    {!! Form::label('image', 'File input') !!}
    <div class="input-group">
      <div class="custom-file">
          <div class="row">
            @if(isset($pic))
              <div class="col-sm-6">
                {!! Form::file('image', null, ['id' => 'exampleInputFile', 'class' => 'custom-file-input', 'required']) !!}
                <!-- <input id="exampleInputFile" type="file" name="image" value="{{$pic->name}}" class="custom-file-input"> -->
                <span class="text-danger">{{$errors->first('title')}}</span>
                {!! Form::label('image', '{{$pic->name}}', ['class' => 'custom-file-label']) !!}
                <!-- <label class="custom-file-label" for="image">{{$pic->name}}</label> -->
                <div class="form-group">
                  {!! Form::label('caption', 'Caption') !!}
                  {!! Form::text('caption', null, ['class' => 'form-control', 'required']) !!} 
                </div>  
              </div>
              <div class="col-sm-6">
                <img src="{{asset('images/slider/'.$pic->name)}}" class="img-fluid img-size">
              </div>
            @else
              <div class="col-sm-12 col-lg-12">
                {!! Form::file('image', null, ['id' => 'exampleInputFile', 'class' => 'custom-file-input', 'required']) !!}
                <!-- <input id="exampleInputFile" type="file" name="image" required class="custom-file-input"> -->
                <span class="text-danger">{{$errors->first('title')}}</span>
                {!! Form::label('image', 'Choose file', ['class' => 'custom-file-label']) !!}
                <!-- <label class="custom-file-label" for="image">Choose file</label> -->
                <div class="form-group">
                  {!! Form::label('caption', 'Caption') !!}
                  {!! Form::text('caption', null, ['class' => 'form-control', 'placeholder' => 'Insert your caption...', 'required']) !!} 
                </div>
              </div>
            @endif
          </div>
      </div>
    </div>
  </div>
</div>
<!-- /.card-body -->
<div class="card-footer">
  {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
</div>