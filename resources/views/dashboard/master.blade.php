<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
@include('dashboard.includes.head')
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    @include('dashboard.includes.header')
    @include('dashboard.includes.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <!-- <div class="col-sm-6">
            <h1 class="m-0 text-dark">Admin Page</h1>
          </div> -->
          @include('dashboard.includes.breadcrumb')
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    @yield('content')
    <!-- /.content -->
  <!-- .content-wrapper --> 
 @include('dashboard.includes.footer')
</div>
<!-- ./wrapper -->
@include('dashboard.includes.script')
</body>
</html>