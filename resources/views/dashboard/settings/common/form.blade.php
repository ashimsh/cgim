<div class="card-body">
  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        {!! Form::label('address', 'Address') !!}
        {!! Form::text('address', null, ['class' => 'form-control', 'required']) !!} 
        <span class="text-danger"> {{$errors->first('address')}} </span>
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
        <span class="text-danger"> {{$errors->first('email')}} </span>
        {!! Form::label('number1', 'Number') !!}
        {!! Form::number('number1', null, ['class' => 'form-control', 'required']) !!}
        <span class="text-danger"> {{$errors->first('number1')}} </span>
        {!! Form::label('number2', 'Number (optional)') !!}
        {!! Form::number('number2', null, ['class' => 'form-control']) !!}
        <span class="text-danger"> {{$errors->first('number2')}} </span>
        {!! Form::label('links', 'Social links') !!}
        {!! Form::text('links', null, ['class' => 'form-control']) !!}
        <span class="text-danger"> {{$errors->first('links')}} </span>
      </div>
    </div>
  </div>
</div>
<!-- /.card-body -->
<div class="card-footer">
    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
</div>