@extends('dashboard.master')

@section('title')
    Settings
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit some settings</h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::model($settings, ['route' => ['dashboard.settings.update',$page->id], 'enctype'=>'multipart/form-data']) !!}
                    @method('PUT')
                    @include('dashboard.settings.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>            
@endsection