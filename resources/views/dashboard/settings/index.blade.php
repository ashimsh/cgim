@extends('dashboard.master')

@section('title')
    Settings
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"> Settings </h3>
            <h6 class="float-right"><a href="{{route('dashboard.settings.create')}}" class="btn btn-info btn-sm">Create a new setting </a> </h6>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                    <tr>
                        <th>Address</th>
                        <th>Email</th>
                        <th>Number-1</th>
                        <th>Number-2</th>
                        <th>Social Network</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($settings as $setting)
                        <tr>
                            <td> {{$setting->address}} </td>
                            <td> {{$setting->email}} </td>
                            <td> {{$setting->number1}} </td>
                            <td> {{$setting->number2}} </td>
                            <td> {!! $setting->links !!} </td>
                            <td>
                                <a href="{{route('dashboard.settings.edit',$settings->id)}}" class="btn btn-primary" title="Edit"> <i class="fa fa-edit"></i> </a>
                                <button class="btn btn-danger" data-toggle = "modal" data-target = "#settings-{{$settings->id}}" title="Delete"><i class="fa fa-trash"></i></button>
                                <div class="modal fade" id="settings-{{$settings->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                {!! Form::model($settings, ['route' => ['dashboard.settings.delete',$settings->id]]) !!}
                                                    @method('DELETE')
                                                     {!! Form::submit('Delete', ['class' => 'btn btn-danger float-right']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection