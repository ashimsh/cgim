
    <div id="table" class="table-editable">
      <span class="table-add float-right mb-3 mr-2"><a href="#!" class="text-success"><i
            class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>
      <table id="example1" class="table table-bordered table-responsive-md table-striped text-center">
        <thead>
          <tr>
            <th class="text-center">Subject</th>
            <th class="text-center">Code</th>
            <th class="text-center">Classification</th>
            <th class="text-center">Credit Hour</th>
            <th class="text-center">Remove</th>
          </tr>
        </thead>
        <tbody>
        @foreach($courses as $course)
          <tr>
            <td class="pt-3-half" contenteditable="true"> {{$course->subject}} </td>
            <td class="pt-3-half" contenteditable="true"> {{$course->code}} </td>
            <td class="pt-3-half" contenteditable="true"> {{$course->classification}} </td>
            <td class="pt-3-half" contenteditable="true"> {{$course->credit_hour}} </td>
            <td>
              <span class="table-remove"><button type="button"
                  class="btn btn-danger btn-rounded btn-sm my-0">Remove</button></span>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    