@extends('dashboard.master')

@section('title')
    Course
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"> Course </h3>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                    <tr>
                        <th>Course for</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($programs as $program)
                        <tr>
                            <td contenteditable="true"> {{$program->title}} </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection