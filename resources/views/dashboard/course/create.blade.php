@extends('dashboard.master')

@section('title')
    Create Course
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::model($programs, ['route' => ['dashboard.courses.store',$programs->id]]) !!}
                        @include('dashboard.course.common.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        const $tableID = $('#table');
        const $BTN = $('#export-btn');
        const $EXPORT = $('#export');

        const newTr = 
        `
            <tr class="hide">
                <td class="pt-3-half" contenteditable="true">Example</td>
                <td class="pt-3-half" contenteditable="true">Example</td>
                <td class="pt-3-half" contenteditable="true">Example</td>
                <td class="pt-3-half" contenteditable="true">Example</td>
                <td>
                    <span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light">Remove</button></span>
                </td>
            </tr>
        `;

        $('.table-add').on('click', 'i', () => {

            const $clone = $tableID.find('tbody tr').last().clone(true).removeClass('hide table-line');

            if ($tableID.find('tbody tr').length === 0) 
            {

                $('tbody').append(newTr);
            }

            $tableID.find('table').append($clone);
        });

        $tableID.on('click', '.table-remove', function () {

            $(this).parents('tr').detach();
        });

        // A few jQuery helpers for exporting only
        jQuery.fn.pop = [].pop;
        jQuery.fn.shift = [].shift;

        $BTN.on('click', () => {

            const $rows = $tableID.find('tr:not(:hidden)');
            const headers = [];
            const data = [];

            // Get the headers (add special header logic here)
            $($rows.shift()).find('th:not(:empty)').each(function () {

                headers.push($(this).text().toLowerCase());
            });

            // Turn all existing rows into a loopable array
            $rows.each(function () {
                const $td = $(this).find('td');
                const h = {};

                // Use the headers from earlier to name our hash keys
                headers.forEach((header, i) => {

                    h[header] = $td.eq(i).text();
                });

                data.push(h);
            });

            // Output the result
            $EXPORT.text(JSON.stringify(data));
        });
    </script>
@endsection