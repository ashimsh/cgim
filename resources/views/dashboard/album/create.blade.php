@extends('dashboard.master')

@section('title')
    New Album
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Add a new Album</h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::open(['route' => 'dashboard.album.store','enctype'=>'multipart/form-data']) !!}
                    @include('dashboard.album.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>
@endsection
