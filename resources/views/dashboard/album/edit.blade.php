@extends('dashboard.master')

@section('title')
    Edit Album
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit this album</h3>
            <h6 class="float-right"><a href="{{route('dashboard.album.show',$album->id)}}" class="btn btn-info btn-sm"> Change the cover image </a> </h6>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::model($album, ['route' => ['dashboard.album.update',$album->id], 'enctype'=>'multipart/form-data']) !!}
                    @method('PUT')
                    @include('dashboard.album.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>            
@endsection