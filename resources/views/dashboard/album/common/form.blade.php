<div class="card-body">
  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', null, ['class' => 'form-control', 'required']) !!} 
        <span class="text-danger"> {{$errors->first('title')}} </span>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control tiny', 'cols' => '30', 'rows' => '10']) !!}
        <span class="text-danger"> {{$errors->first('description')}} </span>
      </div>
    </div>
  </div>
</div>
<!-- /.card-body -->
<div class="card-footer">
    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
</div>