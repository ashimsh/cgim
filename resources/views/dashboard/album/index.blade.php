@extends('dashboard.master')

@section('title')
    Albums
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"> Albums </h3>
            <h6 class="float-right"><a href="{{route('dashboard.album.create')}}" class="btn btn-info btn-sm">Create a new album </a> </h6>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                    <tr>
                        <th>Event name</th>
                        <th>Cover image</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($albums as $album)
                        <tr>
                            <td> <a href="{{route('dashboard.album.show',$album->id)}}"> {{$album->title}} </a> </td>
                            <td> 
                                @if($album->pictures->count() > 0)
                                    @foreach($album->pictures as $picture)
                                        @if($picture->cover == 1) 
                                            <img alt="Cover image" src="{{asset('images/photo-gallery/'.$picture->name)}}" class="w3-border w3-padding" width="200px" height="100px">    
                                        @endif 
                                    @endforeach
                                @else
                                    <a href="{{route('dashboard.pictures.create',$album->id)}}" class="btn btn-primary"> Set up a cover image </a>
                                @endif
                            </td>
                            <td> {{$album->description}} </td>
                            <td>
                                <a href="{{route('dashboard.pictures.create',$album->id)}}" class="btn btn-primary" title="Upload pictures"><i class="fa fa-upload"></i></a>
                                <a href="{{route('dashboard.album.edit',$album->id)}}" class="btn btn-primary" title="Edit"> <i class="fa fa-edit"></i> </a>
                                <button class="btn btn-danger" data-toggle = "modal" data-target = "#album-{{$album->id}}" title="Delete"><i class="fa fa-trash"></i></button>
                                <div class="modal fade" id="album-{{$album->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                {!! Form::model($album, ['route' => ['dashboard.album.delete',$album->id]]) !!}
                                                    @method('DELETE')
                                                     {!! Form::submit('Delete', ['class' => 'btn btn-danger float-right']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection