@extends('dashboard.master')

@section('title')
    {{$album->title}}
@endsection

@section('content')
    <div class="wrap-content">
        @foreach($album->pictures as $key => $picture)
            <div class="responsive">
                <div class="gallery">
                    <a target="_blank">
                        <img src="{{asset('images/photo-gallery/'.$picture->name)}}" width="600" height="400">
                    </a>
                    <div class="form-group">
                        @if($picture->cover == 0)
                            {!! Form::model($picture, ['route' => ['dashboard.pictures.update', $picture->id], 'id'=>'description' ,'class' => 'desc', 'name' => 'coverp']) !!}
                                @method('PUT')
                                {!! Form::submit('Make this cover photo', ['class' => 'btn btn-success btn-sm form-control']) !!}
                                <!-- <button class="btn btn-success btn-sm form-control">Make This A Cover Photo</button> -->
                            {!! Form::close() !!}
                        @endif
                        {!! Form::model($picture, ['route' => ['dashboard.pictures.delete',$picture->id]]) !!}
                            @method('DELETE')
                            {!! Form::button(' <i class="fa fa-trash"></i> ', ['type' => 'submit', 'class' => 'btn btn-danger float-right form-control', 'title' => 'Delete']) !!}
                        {!! Form::close() !!}
                    </div>
                    
                </div>
            </div>
        @endforeach
    </div>
@endsection

<!-- @section('js')
    <script>
        $(document).ready(function(){
            

                $('#check').on('change',function(){
                    $("form[name='coverp']").submit();
                });
            
            
        });
    </script>
@endsection -->