@extends('dashboard.master')

@section('title')
    {{$programs->title}}
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title"> {{$programs->title}} ({{$programs->abbr}}) </h3>
            <div style="float: right">
                <a href="{{route('dashboard.courses.create',$programs->id)}}" class="btn btn-success">Add Course</a>
                <a href="{{route('dashboard.programs.edit',$programs->id)}}" title="Edit" class="btn btn-primary"> <i class="fa fa-edit"></i> </a> 
                <button class="btn btn-danger" data-toggle = "modal" data-target = "#program-{{$programs->id}}" title="Delete"><i class="fa fa-trash"></i></button>
                <div class="modal fade" id="program-{{$programs->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Are you sure?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                {!! Form::model($programs, ['route' => ['dashboard.programs.delete',$programs->id]]) !!}
                                    @method('DELETE')
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger float-right']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    {{$programs->description}} 
                </div>
                <div class="col-sm-6">
                    <img src="{{asset('images/programs/'.$programs->pic_name)}}" class="img-fluid img-size">
                </div>
            </div>
        </div>
    </div>
@endsection    