<div class="card-body">
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <div class="text-center">{!! Form::label('sel1', 'Select level:') !!}</div>
        {!! Form::select('level_id',$levels->pluck('title','id'),null,['class' => 'form-control']) !!}
      </div>
      <div class="form-group">
        <div class="text-center">{!! Form::label('title', 'Title') !!}</div>
        {!! Form::text('title',null, ['class' => 'form-control', 'required']) !!}
        <span class="text-danger">{{$errors->first('title')}}</span>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <div class="text-center"> {!! Form::label('semesters', 'Semesters:') !!} </div>
            {!! Form::selectRange('semesters', 2, 12, 8 , ['class' => 'form-control']) !!}
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <div class="text-center">{!! Form::label('abbr', 'Abbreviation') !!}</div>  
            {!! Form::text('abbr',null, ['class' => 'form-control', 'required']) !!}
            <span class="text-danger">{{$errors->first('abbr')}}</span>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="text-center">{!! Form::label('image', 'File input') !!}</div>
        <div class="input-group">
          <div class="custom-file">
            @if(isset($programs))
              <div class="col-sm-6">
                {!! Form::file('image',null, ['class' => 'custom-file-input', 'id' => 'exampleInputFile']) !!}
                <span class="text-danger">{{$errors->first('image')}}</span>
                {!! Form::label('image', $programs->pic_name, ['class' => 'custom-file-label']) !!}
                <!-- <label class="custom-file-label" for="image">{{$programs->pic_name}}</label> -->
              </div>
              <div class="col-sm-6">
                <img src="{{asset('images/programs/'.$programs->pic_name)}}" class="img-fluid img-size">
              </div>
            @else
              {!! Form::file('image', ['class' => 'custom-file-input form-control', 'id' => 'exampleInputFile']) !!}
              <!-- <input id="exampleInputFile" type="file" name="image" required class="custom-file-input"> -->
              <span class="text-danger">{{$errors->first('image')}}</span>
              <!-- <label class="custom-file-label" for="image">Choose file</label> -->
              {!! Form::label('image', 'Choose file', ['class' => 'custom-file-label']) !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="text-center">{!! Form::label('description', 'Description') !!}</div>
      {!! Form::textarea('description', null, ['class' => 'form-control tiny']) !!}
      <span class="text-danger">{{$errors->first('description')}}</span>
    </div>
  </div>
</div>
 <!-- /.card-body -->
<div class="card-footer">
  {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
</div>