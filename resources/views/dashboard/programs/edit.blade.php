@extends('dashboard.master')

@section('title')
    Edit Program
@endsection

@section('content')
<div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit this program</h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::model($programs, ['route' => ['dashboard.programs.update',$programs->id], 'enctype'=>'multipart/form-data']) !!}
                    @method('PUT')
                    @include('dashboard.programs.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>            
@endsection