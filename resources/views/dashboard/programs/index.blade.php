@extends('dashboard.master')

@section('title')
    Programs
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Programs</h3>
            <h6 class="float-right"><a href="{{route('dashboard.programs.create')}}" class="btn btn-info btn-sm">Upload a new program</a> </h6>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                <tr>
                        <th>Levels</th>
                        <th>Programs</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($levels as $level)
                        <tr>
                            <td> {{$level->title}}  </td>
                            <td>
                                @foreach($level->programs as $program)
                                    <a href="{{route('dashboard.programs.show',$program->id)}}"> {{$program->title}} ({{$program->abbr}}) </a> <hr> 
                                @endforeach  
                            </td>      
                        </tr>
                    @endforeach 
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> 
                        </div>
                    @endif   
                </tbody>
            </table>
        </div>
    </div>
@endsection