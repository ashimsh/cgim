@extends('dashboard.master')

@section('title')
    Add a Program
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Add a new Program</h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::open(['route' => 'dashboard.programs.store','enctype'=>'multipart/form-data']) !!}
                    @include('dashboard.programs.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>
@endsection
