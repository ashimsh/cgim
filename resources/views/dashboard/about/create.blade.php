@extends('dashboard.master')

@section('title')
    About
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Add something in About</h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::open(['route' => ['dashboard.about.store'], 'enctype'=>'multipart/form-data']) !!}
                    @include('dashboard.about.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>
@endsection
