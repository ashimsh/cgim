@extends('dashboard.master')

@section('title')
    About 
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"> About Us </h3>
            <h6 class="float-right"><a href="{{route('dashboard.about.create')}}" class="btn btn-info btn-sm">Add something in about </a> </h6>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($about as $abt)
                        <tr>
                            <td> {{$abt->title}} </td>
                            <td> {!! $abt->description !!} </td>
                            <td>
                                <a href="{{route('dashboard.about.edit',$abt->id)}}" class="btn btn-primary" title="Edit"> <i class="fa fa-edit"></i> </a> <hr>
                                <button class="btn btn-danger" data-toggle = "modal" data-target = "#about-{{$abt->id}}" title="Delete"><i class="fa fa-trash"></i></button>
                                <div class="modal fade" id="about-{{$abt->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                {!! Form::model($abt, ['route' => ['dashboard.about.delete',$abt->id]]) !!}
                                                    @method('DELETE')
                                                     {!! Form::submit('Delete', ['class' => 'btn btn-danger float-right']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection