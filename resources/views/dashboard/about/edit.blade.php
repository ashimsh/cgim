@extends('dashboard.master')

@section('title')
    About
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit this </h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::model($about, ['route' => ['dashboard.about.update',$about->id], 'enctype'=>'multipart/form-data']) !!}
                    @method('PUT')
                    @include('dashboard.about.common.form')
                {!! Form::close() !!}
            </div>      
        </div>
    </div>            
@endsection